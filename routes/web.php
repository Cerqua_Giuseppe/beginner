<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('stockcreate','StockController@create');
Route::post('stockcreate','StockController@store');
Route::get('stocks','StockController@index');
Route::get('/edit/stock/{id}','StockController@edit');
Route::post('/edit/stock/{id}','StockController@update');
Route::delete('/delete/stock/{id}','StockController@destroy');
