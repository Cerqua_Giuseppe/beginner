<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StockController extends Controller
{
    public function create()
    {
        return view('stockcreate');
    }
	
	public function store(Request $request)
    {
        $stock= new \App\Stocks;
        $stock->stockname=$request->get('stockname');
        $stock->stockprice=$request->get('stockprice');
        $stock->description=$request->get('description');
        $stock->save();
        return redirect('stocks')->with('success', 'Stock has been added');
    }
	
	public function index()
    {
        $stocks=\App\Stocks::all();
        return view('stockindex',compact('stocks'));
    }
	
	public function edit($id)
    {
        $stock = \App\Stocks::find($id);
        return view('stockedit',compact('stock','id'));
    }
	
	public function update(Request $request, $id)
    {
        $stock= \App\Stocks::find($id);
        $stock->stockname=$request->get('stockname');
        $stock->stockprice=$request->get('stockprice');
        $stock->description=$request->get('description');
        $stock->save();
        return redirect('stocks')->with('success','Stock has been  updated');
    }
	
	public function destroy($id)
    {
        $stock = \App\Stocks::find($id);
        $stock->delete();
        return redirect('stocks')->with('success','Stock has been  deleted');
    }
}
